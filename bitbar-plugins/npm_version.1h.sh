#!/bin/zsh
export PATH="/usr/local/bin:$PATH"
localVersion="$(npm -v)"
latestVersion="$(npm info npm version)"
color_red=" | color=#ff0000 dropdown=false"
color_yellow=" | color=#ffa500 dropdown=false"
color_green=" | color=#008000 dropdown=false"

if [[ $localVersion == $latestVersion ]]; then
    echo "NPM: $localVersion $color_green"
    echo "---"
    echo "Up to date $latestVersion | color=#008000"
elif [[ $localVersion < $latestVersion ]]; then
    echo "NPM: $localVersion $color_red"
    echo "---"
    echo "Outdated | color=#ff0000"
    echo "Please update to $latestVersion | color=#ff0000 bash='npm i -g npm@latest'"
else
    echo "NPM: $localVersion | color=#ffa500"
    echo "---"
    echo "Up to date $latestVersion | color=#ffa500"
fi

