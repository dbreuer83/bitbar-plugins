#!/bin/zsh
export PATH="/usr/local/bin:$PATH"
localVersion="$(/usr/local/bin/ng -v | grep cli | sed "s/@angular\/cli\://g")"
latestVersion="$(npm info @angular/cli version)"
color_red=" | color=#ff0000 dropdown=false"
color_yellow=" | color=#ffa500 dropdown=false"
color_green=" | color=#008000 dropdown=false"

if [[ $localVersion == $latestVersion ]]; then
    echo "NG: $localVersion $color_green"
    echo "---"
    echo "Up to date | color=#008000"
elif [[ $localVersion < $latestVersion ]]; then
    echo "NG: $localVersion $color_red"
    echo "---"
    echo "Outdated | color=#ff0000"
    echo "Please update to $latestVersion | color=#ff0000 bash='npm i -g @angular/cli@latest'"
else
    echo "NG: $localVersion | color=#ffa500"
    echo "---"
    echo "Up to date | color=#ffa500"
fi