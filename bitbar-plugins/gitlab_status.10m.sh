#!/usr/bin/env bash
# <bitbar.title>Gitlab status plugin</bitbar.title>
# <bitbar.version>v0.1</bitbar.version>
# <bitbar.author>Brett Jones</bitbar.author>
# <bitbar.author.github>blockloop</bitbar.author.github>
# <bitbar.image>https://dl.dropbox.com/s/r4st2fnfhy7tcsv/gitlab-status-screenshot-bitbar.png</bitbar.image>
# <bitbar.desc>Shows the current status of status.gitlab.com. Find out if Gitlab is having DDOS problems which will affect pushes/pulls.</bitbar.desc>
# <bitbar.dependencies>python</bitbar.dependencies>
#
color_red="✘ | color=#ff0000 dropdown=false"
color_yellow="◉ | color=#ffa500 dropdown=false"
color_green="✔︎ | color=#008000 dropdown=false"

rawfeed="$(curl -SsL https://status.gitlab.com/ | grep OK)"
feed="$(echo "$rawfeed" | sed 's/<h5><span class="label label-.*">//g' | sed 's/<\/span>//g' | sed 's/<\/h5>//g')"

if [[ $rawfeed == *"label-error"* ]]; then
    echo "GL: $color_red"
elif [[ $rawfeed == *"label-warning"* ]]; then
    echo "GL: $color_yellow"
else
    echo "GL: $color_green"
fi

echo "---"
while read -r l; do
    echo "$l"
done <<< "$feed"
echo "status.gitlab.com | href=https://status.gitlab.com/"
