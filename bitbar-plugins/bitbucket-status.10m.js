#!/usr/bin/env /usr/local/bin/node
// <bitbar.title>Bitbucket status plugin</bitbar.title>
// <bitbar.version>v0.1</bitbar.version>
// <bitbar.author>David Breuer</bitbar.author>
// <bitbar.author.github>dbreuer83</bitbar.author.github>
// <bitbar.image></bitbar.image>
// <bitbar.desc>Shows the current status of status.bitbucket.org. Find out if Bitbucket is having DDOS problems which will affect pushes/pulls.</bitbar.desc>
// <bitbar.dependencies>python</bitbar.dependencies>

const bitbar = require('bitbar');
const fetch = require('node-fetch');
let AllOk = false;
let statusList = [];
let componentList = [];
// /**
//  * URL: https://bqlf8qjztdtr.statuspage.io/api/v2/components.json
//  */
fetch('https://bqlf8qjztdtr.statuspage.io/api/v2/components.json')
		.then(function(response) { return response.json(); })
		.then(function(j) {
			j.components.map( item => {
				componentList.push({
					text: item.name + ': ' + item.status,
					color: (item.status==='operational') ? 'green' : 'yellow'
				});
				statusList.push((item.status==='operational'));
			});
			AllOk = (statusList.indexOf(false) === -1);
			const BBText = 'BB: ' + ((AllOk) ? '✔' : '𝙭');
			componentList.unshift(bitbar.sep)
			componentList.unshift({
				text: BBText,
				color: (AllOk) ? 'green' : 'yellow'
			});

			bitbar(componentList);
});

