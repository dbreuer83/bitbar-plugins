#!/bin/zsh
# <bitbar.title>Node LTS/Latest version check</bitbar.title>
# <bitbar.version>v0.1</bitbar.version>
# <bitbar.author>David Breuer</bitbar.author>
# <bitbar.author.github>dbreuer</bitbar.author.github>
# <bitbar.desc>Show latest and LTS version for node and using NVM to upgrade.</bitbar.desc>
# <bitbar.dependencies>bash</bitbar.dependencies>
#
export PATH="/usr/local/bin:$PATH"
localVersion="$(node -v | sed s/v//g)"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm

do_version_check() {

   [ "$1" == "$2" ] && return 10

   ver1front=`echo $1 | cut -d "." -f -1`
   ver1back=`echo $1 | cut -d "." -f 2-`

   ver2front=`echo $2 | cut -d "." -f -1`
   ver2back=`echo $2 | cut -d "." -f 2-`

   if [ "$ver1front" != "$1" ] || [ "$ver2front" != "$2" ]; then
       [ "$ver1front" -gt "$ver2front" ] && return 11
       [ "$ver1front" -lt "$ver2front" ] && return 9

       [ "$ver1front" == "$1" ] || [ -z "$ver1back" ] && ver1back=0
       [ "$ver2front" == "$2" ] || [ -z "$ver2back" ] && ver2back=0
       do_version_check "$ver1back" "$ver2back"
       return $?
   else
           [ "$1" -gt "$2" ] && return 11 || return 9
   fi
} 

latestLTSVersion="$(nvm version-remote --lts | sed s/v//g)"
latestVersion="$(nvm version-remote | sed s/v//g)"
color_red=" | color=#ff0000 dropdown=false"
color_yellow=" | color=#ffa500 dropdown=false"
color_green=" | color=#008000 dropdown=false"

local i ver1=($localVersion) ver2=($latestVersion)

if [[ $localVersion < $latestVersion && $localVersion == $latestLTSVersion ]]; then
    echo "N: $localVersion $color_green"
    echo "---"
    echo "Outdated Latest | color=#ff0000"
    echo "Please update to $latestVersion | color=#ff0000 bash='nvm install --reinstall-packages-from=$localVersion'"
    echo "Up to date LTS  | color=#008000"
elif [[ $localVersion < $latestVersion && $localVersion < $latestLTSVersion ]]; then
    echo "N: $localVersion $color_red"
    echo "---"
    echo "Outdated Latest | color=#ff0000"
    echo "Please update to $latestVersion | color=#ff0000 bash='nvm install --reinstall-packages-from=$localVersion'"
    echo "Outdated LTS | color=#ff0000"
    echo "Please update to $latestLTSVersion | color=#ff0000 bash='nvm install --lts --reinstall-packages-from=$localVersion'"
else
    echo "N: $localVersion | color=#ffa500"
    echo "---"
    echo "Up to date LTS $latestLTSVersion | color=#ffa500"
    echo "Up to date Latest $latestVersion | color=#ffa500"
fi