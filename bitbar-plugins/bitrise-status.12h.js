#!/usr/bin/env /usr/local/bin/node

const bitbar = require('bitbar');
const fetch = require('node-fetch');
const { exec } = require('child_process');

let AllOk = false;
let statusList = [];
let componentList = [];
// /**
//  * URL: https://status.bitrise.io/api/v2/components.json
//  */
exec('/usr/local/bin/bitrise -v', (err, stdout, stderr) => {
	if (err) {
		return;
	}

	componentList.push({
		text: 'Bitrise CLI: ' + stdout,
		color: bitbar.darkMode ? 'green' : 'blue',
	});
	componentList.push(bitbar.sep)
});
	fetch('http://status.bitrise.io/api/v2/components.json')
		.then(function (response) { return response.json(); })
		.then(function (j) {
			j.components.map(item => {
				componentList.push({
					text: item.name + ': ' + item.status,
					color: (item.status === 'operational') ? 'green' : 'yellow'
				});
				statusList.push((item.status === 'operational'));
			});
			AllOk = (statusList.indexOf(false) === -1);
			const BitriseText = 'BR: ' + ((AllOk) ? '✔' : '𝙭');
			componentList.unshift(bitbar.sep)
			componentList.unshift({
				text: BitriseText,
				color: (AllOk) ? 'green' : 'yellow',
				dropdown: false
			});
			bitbar(componentList);
		});




